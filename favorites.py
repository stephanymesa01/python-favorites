# You go girl!
first_question = "What is your favorite color?"

first_question_answer = "Blue"

second_question = "What is your favorite food?"
second_question_answer = "Pizza"

third_question = "Who is your favorite fictional character?"
third_question_answer = "Winnie"

fourth_question = "What is your favorite animal?"
fourth_question_answer = "Dog"

fifth_question = "What is your favorite programming language?"
fifth_question_answer = "Python"

# This exercise is all about adding, checking status, commits, and pushes!
